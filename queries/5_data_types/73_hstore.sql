CREATE EXTENSION hstore;

CREATE TABLE books (
	id serial primary key,
	title VARCHAR (255),
	attr hstore
);

INSERT INTO books (title, attr)
VALUES
	(
		'PostgreSQL Tutorial',
		'"paperback" => "243",
	   "publisher" => "postgresqltutorial.com",
	   "language"  => "English",
	   "ISBN-13"   => "978-1449370000",
		 "weight"    => "11.2 ounces"'
	);

SELECT
	attr -> 'ISBN-13' AS isbn
FROM
	books;

SELECT
	attr -> 'weight' AS weight
FROM
	books
WHERE
	attr -> 'ISBN-13' = '978-1449370000';

UPDATE books
SET attr = attr || '"freeshipping"=>"yes"' :: hstore;

SELECT
	title,
    attr -> 'freeshipping' AS freeshipping
FROM
	books;

UPDATE books
SET attr = attr || '"freeshipping"=>"no"' :: hstore;

UPDATE books 
SET attr = delete(attr, 'freeshipping');

SELECT
  title,
  attr->'publisher' as publisher,
  attr
FROM
	books
WHERE
	attr ? 'publisher';

SELECT
	title
FROM
	books
WHERE
	attr @> '"weight"=>"11.2 ounces"' :: hstore;

SELECT
	title
FROM
	books
WHERE
	attr ?& ARRAY [ 'language', 'weight' ];

SELECT
	akeys (attr)
FROM
	books;

SELECT
	skeys (attr)
FROM
	books;

SELECT
	avals (attr)
FROM
	books;

SELECT
	svals (attr)
FROM
	books;

SELECT
  title,
  hstore_to_json (attr) json
FROM
  books;

SELECT
	title,
	(EACH(attr) ).*
FROM
	books;