CREATE TABLE documents (
	document_id serial PRIMARY KEY,
	header_text VARCHAR (255) NOT NULL,
	posting_date DATE NOT NULL DEFAULT CURRENT_DATE
);

SELECT NOW()::date;

SELECT CURRENT_DATE;

SELECT TO_CHAR(NOW() :: DATE, 'dd/mm/yyyy');

SELECT TO_CHAR(NOW() :: DATE, 'Mon dd, yyyy');

-- Get the interval between two dates
SELECT
	first_name,
	last_name,
	now() - hire_date as diff
FROM employees;

-- Calculate ages in years, months, and days
SELECT
	employee_id,
	first_name,
	last_name,
	AGE(birth_date)
FROM employees;