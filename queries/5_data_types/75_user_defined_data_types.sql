CREATE TABLE mail_list (
    ID SERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    CHECK (
        first_name !~ '\s'
        AND last_name !~ '\s'
    )
);

CREATE DOMAIN contact_name AS 
    VARCHAR NOT NULL CHECK (value !~ '\s');

CREATE TABLE mail_list (
    id serial PRIMARY KEY,
    first_name contact_name,
    last_name contact_name,
    email VARCHAR NOT NULL
);

INSERT INTO mail_list (first_name, last_name, email)
VALUES
    (
        'Jame V',
        'Doe',
        'jame.doe@example.com'
    );

CREATE TYPE film_summary AS (
    film_id INT,
    title VARCHAR,
    release_year YEAR
); 

CREATE OR REPLACE FUNCTION get_film_summary (f_id INT) 
    RETURNS film_summary AS 
$$ 
SELECT
    film_id,
    title,
    release_year
FROM
    film
WHERE
    film_id = f_id ; 
$$ 
LANGUAGE SQL;