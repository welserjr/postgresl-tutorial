CREATE TABLE contacts (
	id serial PRIMARY KEY,
	name VARCHAR (100),
	phones TEXT []
);

INSERT INTO contacts (name, phones)
VALUES
	(
		'John Doe',
		ARRAY [ '(408)-589-5846',
		'(408)-589-5555' ]
	);

INSERT INTO contacts (name, phones)
VALUES
	(
		'Lily Bush',
		'{"(408)-589-5841"}'
	),
	(
		'William Gate',
		'{"(408)-589-5842","(408)-589-58423"}'
	);

SELECT
	name,
	phones [ 1 ]
FROM contacts;

SELECT 	name
FROM contacts
WHERE phones [ 2 ] = '(408)-589-58423';

UPDATE contacts
SET phones [ 2 ] = '(408)-589-5843'
WHERE ID = 3;

UPDATE contacts
SET phones = '{"(408)-589-5843"}'
WHERE ID = 3;