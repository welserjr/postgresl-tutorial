CREATE TABLE stock_availability (
	product_id INT NOT NULL PRIMARY KEY,
	available BOOLEAN NOT NULL
);

CREATE TABLE boolean_demo(
   ...
   is_ok BOOL DEFAULT 't'
);