select pg_relation_size('actor');

SELECT pg_size_pretty (pg_relation_size('actor'));

SELECT pg_size_pretty (pg_total_relation_size ('actor'));

SELECT
    relname AS "relation",
    pg_size_pretty (pg_total_relation_size (C .oid)) AS "total_size"
FROM pg_class C
LEFT JOIN pg_namespace N ON (N.oid = C .relnamespace)
WHERE nspname NOT IN ('pg_catalog', 'information_schema')
AND C .relkind <> 'i'
AND nspname !~ '^pg_toast'
ORDER BY pg_total_relation_size (C .oid) DESC
LIMIT 5;

SELECT pg_size_pretty (pg_database_size ('dvdrental'));

SELECT
    pg_database.datname,
    pg_size_pretty(pg_database_size(pg_database.datname)) AS size
FROM pg_database;

SELECT pg_size_pretty (pg_indexes_size('actor'));

SELECT pg_size_pretty (pg_tablespace_size ('pg_default'));

select pg_column_size(5::smallint);
select pg_column_size(5::int);
select pg_column_size(5::bigint);