CREATE DATABASE targetdb 
WITH TEMPLATE sourcedb;

CREATE DATABASE dvdrental_test 
WITH TEMPLATE dvdrental;

pg_dump -U postgres -O dvdrental dvdrental.sql
CREATE DATABASE dvdrental;
psql -U postgres -d dvdrental -f dvdrental.sql

pg_dump -C -h localhost -U postgres dvdrental | psql -h remote -U postgres dvdrental