CREATE DATABASE db_name
 OWNER =  role_name
 TEMPLATE = template
 ENCODING = encoding
 LC_COLLATE = collate
 LC_CTYPE = ctype
 TABLESPACE = tablespace_name
 CONNECTION LIMIT = max_concurrent_connection

CREATE DATABASE testdb1;

CREATE DATABASE hrdb
 WITH ENCODING='UTF8'
 OWNER=hr
 CONNECTION LIMIT=25;