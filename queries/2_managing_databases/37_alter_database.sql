	
ALTER DATABASE target_database OWNER TO new_onwer;

ALTER DATABASE target_database SET TABLESPACE new_tablespace;

CREATE DATABASE testdb2;
ALTER DATABASE testdb2 RENAME TO testhrdb;
ALTER DATABASE testhrdb OWNER TO hr;

CREATE ROLE hr VALID UNTIL 'infinity';

ALTER DATABASE testhrdb
SET TABLESPACE hr_default;

CREATE TABLESPACE hr_default
 OWNER hr
 LOCATION E'C:\\pgdata\\hr';

ALTER DATABASE testhrdb SET escape_string_warning TO off;