ALTER SCHEMA schema_name 
RENAME TO new_name;

ALTER SCHEMA schema_name 
OWNER TO { new_owner | CURRENT_USER | SESSION_USER};