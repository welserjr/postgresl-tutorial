CREATE SCHEMA [IF NOT EXISTS] schema_name;

-- You can also create a schema for a user:
CREATE SCHEMA [IF NOT EXISTS] AUTHORIZATION user_name;

-- PostgreSQL also allows you to create a schema and a list of objects such as tables and 
-- views using a single statement as follows:
CREATE SCHEMA schema_name
    CREATE TABLE table_name1 (...)
    CREATE TABLE table_name2 (...)
    CREATE VIEW view_name1
        SELECT select_list FROM table_name1;