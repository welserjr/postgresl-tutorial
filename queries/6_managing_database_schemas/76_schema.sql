SELECT current_schema();

CREATE SCHEMA sales;

SET search_path TO sales, public;

SELECT * FROM sales.staff;