DROP SCHEMA [IF EXISTS] schema_name [ CASCADE | RESTRICT ];

DROP SCHEMA [IF EXISTS] schema_name1 [,schema_name2,...] 
[CASCADE | RESTRICT]