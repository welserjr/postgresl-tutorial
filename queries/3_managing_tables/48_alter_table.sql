-- To add a new column to a table, you use ALTER TBLE ADD COLUMN statement:
ALTER TABLE table_name ADD COLUMN new_column_name TYPE;

-- To remove an existing column, you use ALTER TABLE DROP COLUMN statement:
ALTER TABLE table_name DROP COLUMN column_name;

-- To rename an existing column, you use the ALTER TABLE RENAME COLUMN TO statement:
ALTER TABLE table_name RENAME COLUMN column_name TO new_column_name;

-- To change a default value of the column, you use ALTER TABLE ALTER COLUMN SET DEFAULT or DROP DEFAULT:
ALTER TABLE table_name ALTER COLUMN column_name [SET DEFAULT value | DROP DEFAULT];

-- To change the NOT NULL constraint, you use ALTER TABLE ALTER COLUMN statement:
ALTER TABLE table_name ALTER COLUMN column_name [SET NOT NULL| DROP NOT NULL];

-- To add a CHECKconstraint, you use ALTER TABLE ADD CHECK statement:
ALTER TABLE table_name ADD CHECK expression;

-- To add a constraint, you use ALTER TABLE ADD CONSTRAINT statement:
ALTER TABLE table_name ADD CONSTRAINT constraint_name constraint_definition;

-- To rename a table you use ALTER TABLE RENAME TO statement:
ALTER TABLE table_name RENAME TO new_table_name;