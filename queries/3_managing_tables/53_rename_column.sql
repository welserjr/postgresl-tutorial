ALTER TABLE table_name 
RENAME COLUMN column_name TO new_column_name;

ALTER TABLE table_name 
RENAME column_name TO new_column_name;

ALTER TABLE table_name
RENAME column_name_1 TO new_column_name_1;

ALTER TABLE table_name
RENAME column_name_2 TO new_column_name_2;