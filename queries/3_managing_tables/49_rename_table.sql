ALTER TABLE table_name
RENAME TO new_table_name;

ALTER TABLE IF EXISTS table_name
RENAME TO new_table_name;