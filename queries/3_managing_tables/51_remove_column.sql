ALTER TABLE table_name 
DROP COLUMN column_name;

ALTER TABLE table_name 
DROP COLUMN column_name CASCADE;

ALTER TABLE table_name 
DROP COLUMN IF EXISTS column_name;

ALTER TABLE table_name
DROP COLUMN column_name_1,
DROP COLUMN column_name_2,
...;