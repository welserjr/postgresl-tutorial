ALTER TABLE table_name
ALTER COLUMN column_name [SET DATA] TYPE new_data_type;

ALTER TABLE table_name
ALTER COLUMN column_name_1 [SET DATA] TYPE new_data_type,
ALTER COLUMN column_name_2 [SET DATA] TYPE new_data_type,
...;