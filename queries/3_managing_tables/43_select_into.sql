SELECT
    film_id,
    title,
    rental_rate
INTO TABLE film_r
FROM film
WHERE rating = 'R' AND rental_duration = 5
ORDER BY title;

SELECT
    film_id,
    title,
    length 
INTO TEMP TABLE short_film
FROM film
WHERE length < 60
ORDER BY title;