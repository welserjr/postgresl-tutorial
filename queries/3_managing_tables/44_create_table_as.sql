CREATE TABLE new_table_name ( column_name_list)
AS query;

CREATE TABLE IF NOT EXISTS new_table_name
AS query;

CREATE TABLE action_film AS
SELECT
    film_id,
    title,
    release_year,
    length,
    rating
FROM film
INNER JOIN film_category USING (film_id)
WHERE category_id = 1 ; -- action

CREATE TABLE IF NOT EXISTS film_rating (rating, film_count) 
AS 
SELECT
    rating,
    COUNT (film_id)
FROM film
GROUP BY rating;