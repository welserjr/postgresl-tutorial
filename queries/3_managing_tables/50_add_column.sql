ALTER TABLE table_name
ADD COLUMN new_column_name data_type;

ALTER TABLE table_name
ADD COLUMN new_column_name_1 data_type constraint,
ADD COLUMN new_column_name_2 data_type constraint;

ALTER TABLE customers 
ADD COLUMN phone VARCHAR;

ALTER TABLE custoemr 
 ADD COLUMN fax VARCHAR,
 ADD COLUMN email VARCHAR;


ALTER TABLE customers 
ADD COLUMN contact_name VARCHAR NOT NULL;

-- ERROR:  column "contact_name" contains null values

ALTER TABLE customers 
ADD COLUMN contact_name VARCHAR;

UPDATE customers
SET contact_name = 'John Doe'
WHERE
	ID = 1;

UPDATE customers
SET contact_name = 'Mary Doe'
WHERE
	ID = 2;

UPDATE customers
SET contact_name = 'Lily Bush'
WHERE
	ID = 3;

ALTER TABLE customers
ALTER COLUMN contact_name SET NOT NULL;

-- OR

ALTER TABLE customers
ADD COLUMN contact_name NOT NULL DEFAULT 'foo';

ALTER TABLE customers 
ALTER COLUMN contact_name 
DROP DEFAULT;