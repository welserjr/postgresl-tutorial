CREATE TABLE employees (
	id serial PRIMARY KEY,
	first_name VARCHAR (50),
	last_name VARCHAR (50),
	birth_date DATE CHECK (birth_date > '1900-01-01'),
	joined_date DATE CHECK (joined_date > birth_date),
	salary numeric CHECK(salary > 0)
);

ALTER TABLE prices_list ADD CONSTRAINT price_discount_check CHECK (
	price > 0
	AND discount >= 0
	AND price > discount
);