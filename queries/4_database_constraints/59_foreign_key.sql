CREATE TABLE so_headers (
	id SERIAL PRIMARY KEY,
	customer_id INTEGER,
	ship_to VARCHAR (255)
);

CREATE TABLE so_items (
	item_id INTEGER NOT NULL,
	so_id INTEGER,
	product_id INTEGER,
	qty INTEGER,
	net_price NUMERIC,
	PRIMARY KEY (item_id, so_id),
	FOREIGN KEY (so_id) REFERENCES so_headers (id)
);

CREATE TABLE child_table(
  c1 INTEGER PRIMARY KEY,
  c2 INTEGER,
  c3 INTEGER,
  FOREIGN KEY (c2, c3) REFERENCES parent_table (p1, p2)
);

ALTER TABLE child_table 
ADD CONSTRAINT constraint_name FOREIGN KEY (c1) REFERENCES parent_table (p1);

ALTER TABLE child_table
ADD CONSTRAINT constraint_fk
FOREIGN KEY (c1)
REFERENCES parent_table(p1)
ON DELETE CASCADE;