CREATE TABLE TABLE (
	column_1 data_type PRIMARY KEY,
	column_2 data_type,
	…
);

CREATE TABLE TABLE (
	column_1 data_type,
	column_2 data_type,
	… 
        PRIMARY KEY (column_1, column_2)
);

CONSTRAINT constraint_name PRIMARY KEY(column_1, column_2,...);

ALTER TABLE table_name ADD PRIMARY KEY (column_1, column_2);

ALTER TABLE vendors ADD COLUMN ID SERIAL PRIMARY KEY;

ALTER TABLE table_name DROP CONSTRAINT primary_key_constraint;