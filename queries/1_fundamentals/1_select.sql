/* Using SELECT statement to query data from one column */
SELECT first_name
FROM customer;

/* Using SELECT statement to query data from multiple columns */
SELECT first_name, last_name, email
FROM customer;

/* Using SELECT statement to query data in all columns of a table */
SELECT *
FROM customer;

/* Using SELECT statement with expressions */
SELECT
	first_name || ' ' || last_name AS full_name,
	email
FROM
	customer;

/* Using SELECT statement with  only expressions */
SELECT 5 * 3 AS result;