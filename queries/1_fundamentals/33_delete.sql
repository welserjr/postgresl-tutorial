DELETE FROM link
WHERE id = 8;

DELETE FROM link 
USING link_tmp
WHERE link.id = link_tmp.id;

DELETE FROM link;

DELETE FROM link_tmp 
RETURNING *;