INSERT INTO link (url, name)
VALUES
   ('https://www.postgresqltutorial.com','PostgreSQL Tutorial');

INSERT INTO link (url, name)
VALUES
 ('http://www.google.com','Google'),
 ('http://www.yahoo.com','Yahoo'),
 ('http://www.bing.com','Bing');

INSERT INTO link_tmp 
SELECT
   *
FROM
   link
WHERE
   last_update IS NOT NULL;