FROM postgres:11.3
MAINTAINER WelserJr welser.m.r@gmail.com

ADD init.sh /docker-entrypoint-initdb.d/
WORKDIR /usr/src/postgres
COPY . /usr/src/postgres