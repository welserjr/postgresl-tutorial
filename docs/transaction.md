## What is a database transaction
A database transaction is a single unit of work which may consist of one or more operations.

A classical example of a transaction is a transfer from one bank account to another. A complete transaction must ensure subtracting an amount from the sender’s account and adding that same amount to the receiver’s account.

A transaction in PostgreSQL is atomic, consistent, isolated, and durable. These properties are often referred to as ACID:

* Atomicity guarantees that the transaction completes in an all-or-nothing manner.
* Consistency ensures the change to data written to the database must be valid and follow predefined rules.
* Isolation determines how transaction integrity is visible to other transactions.
* Durability makes sure that transactions which have been committed will be stored permanently.